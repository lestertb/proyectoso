from multiprocessing import Pool 
import os
import cv2
from datetime import datetime
import numpy as np
import psutil
from moviepy.editor import *
from collections import Counter

#Globals
VIDEO_EXTENSION = ('.mp4','.vlc') # extensione permitidas para analizar
CPU_COUNT = psutil.cpu_count() - 1 # cantidad de nucleos del ordenador
DIRECTORY = 'input/'
RESULT_DIRECTORY = 'temp/'
OUTPUT_DIRECTORY = 'output/'
MAX_VIDEO_DURATION = 8
ANALYZE_VIDEO = True
LOTE_SIZE = 50

#Method to calculate seconds
def calculateSeconds(pathVideoTSeconds):
    print(pathVideoTSeconds)
    cap = cv2.VideoCapture(pathVideoTSeconds)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = frame_count/fps
    seconds = duration % 60
    cap.release()
    cv2.destroyAllWindows()
    return seconds


# Method to divide the original video
def divideVideo(filename):
    print(filename)
    path = "{}{}".format(DIRECTORY,filename)
    name = filename.split('.')
    extension = name.pop()
    name = name[0]
    #Calculate seconds video
    totalSeconds = calculateSeconds(path)
    video = VideoFileClip(path)
    #Divide video
    initalSecond = 0
    count = 0
    while MAX_VIDEO_DURATION < totalSeconds:
        editado = video.subclip(initalSecond,initalSecond+MAX_VIDEO_DURATION)
        initalSecond += MAX_VIDEO_DURATION
        totalSeconds -= MAX_VIDEO_DURATION
        editado.write_videofile("{}{}-{}.{}".format(RESULT_DIRECTORY,name,count,extension),codec='libx264')
        count += 1

    if MAX_VIDEO_DURATION > totalSeconds and totalSeconds > 0:
        editado = video.subclip(initalSecond,initalSecond+totalSeconds)
        editado.write_videofile("{}{}-{}.{}".format(RESULT_DIRECTORY,name,count,extension),codec='libx264')

# metodo para covertir videos en imagenes
def convertVideoToImage(filename):
    path = "{}{}".format(DIRECTORY,filename)
    name = filename.split('.')[0]

    vidcap = cv2.VideoCapture(path)
    success,image = vidcap.read()
    count = 0
    jumpNext = False
    success = True
    while success:
        if(jumpNext): # salta una image por medio para reducir la cantidad
            success,image = vidcap.read()
            jumpNext = False
            continue
        success,image = vidcap.read()
        cv2.imwrite("{}{}-{}.{}".format(RESULT_DIRECTORY,name,count,'jpeg'), image)     # save frame as JPEG file
        count += 1
        jumpNext = True

def processVideos():
    videos = []
    for file in os.listdir(DIRECTORY): #por cada archivo de la carpeta
        if file.endswith(VIDEO_EXTENSION): # verifico si es un video valido
            videos.append(file) 
    
    with Pool(CPU_COUNT) as p: # hace subclips de lso videos
        if(ANALYZE_VIDEO):
            p.map(divideVideo, videos)
        else:
            p.map(convertVideoToImage, videos)    
    return os.listdir(RESULT_DIRECTORY)

# Method to show in real-time yolo detection
def detectVideo(filename):
    diccionario = {}
    try:
        pathVideo = "{}{}".format(RESULT_DIRECTORY,filename)
        net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
        classes = []
        with open("obj.names", "r") as f:
            for line in f.readlines():
                classes.append(line.strip())
                diccionario[line.strip()] = 0

        layer_names = net.getLayerNames()
        output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
        colors = np.random.uniform(0, 255, size=(len(classes), 3))

        if pathVideo == "":
            pathVideo = 0
        cap = cv2.VideoCapture(pathVideo)

        while True:
            ret, img = cap.read()
            if ret == True:
                height, width, channels = img.shape
                # width = 512
                # height = 512

            # Detecting objects
            blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)

            net.setInput(blob)
            outs = net.forward(output_layers)

            # Showing information on the screen
            class_ids = []
            confidences = []
            boxes = []
            for out in outs:
                for detection in out:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    if confidence > 0.5:
                        # Object detected
                        center_x = int(detection[0] * width)
                        center_y = int(detection[1] * height)
                        w = int(detection[2] * width)
                        h = int(detection[3] * height)

                        # Rectangle coordinates
                        x = int(center_x - w / 2)
                        y = int(center_y - h / 2)

                        boxes.append([x, y, w, h])
                        confidences.append(float(confidence))
                        class_ids.append(class_id)

            indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)

            font = cv2.FONT_HERSHEY_PLAIN
            for i in range(len(boxes)):
                if i in indexes:
                    x, y, w, h = boxes[i]
                    label = str(classes[class_ids[i]])
                    diccionario[label] += 1
                    #print(label, "detectado")
                    color = colors[class_ids[i]]
                    cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                    cv2.putText(img, label, (x, y + 30), font, 3, color, 3)

            # frame = cv2.resize(img, (width, height), interpolation=cv2.INTER_AREA)
            img = cv2.resize(img,(640,360),fx=0,fy=0, interpolation = cv2.INTER_CUBIC)
            cv2.imshow("Image", img)
            key = cv2.waitKey(1)
            if key == 27:
                break
        cap.release()
        cv2.destroyAllWindows()
        return diccionario
    except:
        print("Fin ShowVideo")
        return diccionario

# Method to show in real-time yolo detection
def detectImage(lote):
    actualDate = datetime.now()
    length = len(lote)
    print("Lote: ",length)
    # Load Yolo
    # Download weight file:https://drive.google.com/file/d/10uJEsUpQI3EmD98iwrwzbD4e19Ps-LHZ/view?usp=sharing
    net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
    classes = []
    diccionario = {}
    with open("obj.names", "r") as f:
        for line in f.readlines():
            classes.append(line.strip())
            diccionario[line.strip()] = 0

    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    colors = np.random.uniform(0, 255, size=(len(classes), 3))
    
    for image in lote:
        path = "{}{}".format(RESULT_DIRECTORY,image)
        img = cv2.imread(path)
        height, width, channels = img.shape
        blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
        net.setInput(blob)
        outs = net.forward(output_layers)

        class_ids = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    # Object detected
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)

                    # Rectangle coordinates
                    x = int(center_x - w / 2)
                    y = int(center_y - h / 2)

                    boxes.append([x, y, w, h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)
        indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
        font = cv2.FONT_HERSHEY_PLAIN
        endDate = datetime.now()
        resultDate = endDate - actualDate
        if(resultDate.seconds > 1):
            actualDate = endDate
            for i in range(len(boxes)):
                if i in indexes:
                    x, y, w, h = boxes[i]
                    label = str(classes[class_ids[i]])
                    diccionario[label] += 1
                    color = colors[class_ids[i]]
                    cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                    cv2.putText(img, label, (x, y + 30), font, 3, color, 3)
                    print("Object detected in frame")
                cv2.imwrite("{}{}".format(OUTPUT_DIRECTORY,image), img)
    return diccionario

def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i:i + n]



#clean delete temp
def cleanTemp(lote):
    for file in lote:
        os.remove(RESULT_DIRECTORY + file)


#print Diccionario
def printDicc(result):
    previus ={}
    for item in result:
        if previus is None:
            previus = item
        else:
            previus = dict(Counter(previus)+Counter(item))
    print(previus)


if __name__ == '__main__':
    #clean temp
    queueTemp = list(chunks(os.listdir(RESULT_DIRECTORY), LOTE_SIZE ))
    with Pool(CPU_COUNT) as subProcess:
        subProcess.map(cleanTemp,queueTemp)
    queue = processVideos()
    if(len(queue) > 0):
        with Pool(CPU_COUNT) as subProcess:
            print("Iniciando Analisis ")
            starDate = datetime.now()
            if(ANALYZE_VIDEO):
                result = subProcess.map(detectVideo,queue)
            else:
                #divido en lotes las imagenes
                result = list(chunks(queue, LOTE_SIZE ))
                result = subProcess.map(detectImage,result)
            endDate = datetime.now()
            printDicc(result)
            result = endDate-starDate
            print("Fin del Analisis en ", result.seconds,"s")
        