import cv2
import numpy as np
import time
import threading
from moviepy.editor import *


#Method to calculate seconds
def calculateSeconds(pathVideoTSeconds):
    cap = cv2.VideoCapture(pathVideoTSeconds)
    fps = cap.get(cv2.CAP_PROP_FPS)
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = frame_count/fps
    seconds = duration % 60
    cap.release()
    cv2.destroyAllWindows()
    return seconds


# Method to divide the original video
def divideVideo(pathVideoToDivide):
    #Calculate seconds video
    totalSeconds = calculateSeconds(pathVideoToDivide)
    #Divide video
    refvideo = (totalSeconds / 2)
    video = VideoFileClip(pathVideoToDivide)
    editado = video.subclip(0,refvideo)
    editado2 = video.subclip(refvideo,totalSeconds)
    editado.write_videofile('TestCustomYOLO/temp/part1.mp4',codec='libx264')
    editado2.write_videofile('TestCustomYOLO/temp/part2.mp4',codec='libx264')


#--------------------------------------------------!-------------------------------------------------#
# Method to show in real-time yolo detection
def showDetection(pathVideo):
    # Load Yolo
    # Download weight file:https://drive.google.com/file/d/10uJEsUpQI3EmD98iwrwzbD4e19Ps-LHZ/view?usp=sharing
    net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
    classes = []
    with open("obj.names", "r") as f:
         classes = [line.strip() for line in f.readlines()]

    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    colors = np.random.uniform(0, 255, size=(len(classes), 3))

    # Loading image
    # img = cv2.imread("room_ser.jpg")
    # img = cv2.resize(img, None, fx=0.4, fy=0.4)

    # Enter file name for example "ak47.mp4" or press "Enter" to start webcam
    val = pathVideo
    if val == "":
        val = 0
    # for video capture
    cap = cv2.VideoCapture(val)
        
    # val = cv2.VideoCapture()
    while True:
        ret, img = cap.read()
        if ret == True:
            height, width, channels = img.shape
            # width = 512
            # height = 512

        # Detecting objects
        blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)

        net.setInput(blob)
        outs = net.forward(output_layers)

        # Showing information on the screen
        class_ids = []
        confidences = []
        boxes = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    # Object detected
                    center_x = int(detection[0] * width)
                    center_y = int(detection[1] * height)
                    w = int(detection[2] * width)
                    h = int(detection[3] * height)

                    # Rectangle coordinates
                    x = int(center_x - w / 2)
                    y = int(center_y - h / 2)

                    boxes.append([x, y, w, h])
                    confidences.append(float(confidence))
                    class_ids.append(class_id)

        indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
        #print(indexes)
        if indexes == 0: print("Object detected in frame")
        font = cv2.FONT_HERSHEY_PLAIN
        for i in range(len(boxes)):
            if i in indexes:
                x, y, w, h = boxes[i]
                label = str(classes[class_ids[i]])
                color = colors[class_ids[i]]
                cv2.rectangle(img, (x, y), (x + w, y + h), color, 2)
                cv2.putText(img, label, (x, y + 30), font, 3, color, 3)

        # frame = cv2.resize(img, (width, height), interpolation=cv2.INTER_AREA)
        img = cv2.resize(img,(640,360),fx=0,fy=0, interpolation = cv2.INTER_CUBIC)
        cv2.imshow("Image", img)
        key = cv2.waitKey(1)
        if key == 27:
            break
    cap.release()
    cv2.destroyAllWindows()

#--------------------------------------------------!-------------------------------------------------#


# Method to generate the results of yolo detection in background
def resultsDetection(pathVideo, pathResult):
    start_time = time.time()
    cap = cv2.VideoCapture(pathVideo)
    whT = 320
    confThreshold = 0.5
    nmsThreshold = 0.3

    classesFile = 'obj.names'
    classNames = []

    with open(classesFile, 'rt') as f:
        classNames = f.read().rstrip('\n').split('\n')

    modelConfiguration = 'yolov3.cfg'
    modelWeights = 'yolov3.weights'

    net = cv2.dnn.readNetFromDarknet(modelConfiguration, modelWeights)
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

    lista = []
    def findObjects(outputs, img):
        hT, wT, cT = img.shape
        bbox = []
        classIds = []
        confs = []
        tiempo = 0
        nombre = ""
        for output in outputs:
            for det in output:
                scores = det[5:]
                classId = np.argmax(scores)
                confidence = scores[classId]

                if confidence > confThreshold:
                    w, h = int(det[2] * wT), int(det[3] * hT)
                    x, y = int((det[0] * wT) - w / 2), int((det[1] * hT) - h / 2)
                    bbox.append([x, y, w, h])
                    classIds.append(classId)
                    confs.append(float(confidence))

        indices = cv2.dnn.NMSBoxes(bbox, confs, confThreshold, nmsThreshold)
        for i in indices:
            i = i[0]
            box = bbox[i]
            x, y, w, h = box[0], box[1], box[2], box[3]
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 135, 0), 2)

            time_milli = cap.get(cv2.CAP_PROP_POS_MSEC)
            if classNames[classIds[i]] != "":
                tiempo = round(time_milli / 1000)
                if tiempo not in lista:
                    nombre = classNames[classIds[i]]
                    cv2.putText(img, f'{classNames[classIds[i]].upper()} {int(confs[i] * 100)}%',
                                (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 209, 0), 2)

        return tiempo, nombre

    aux = 1;
    contador = 0
    while True:
        success, img = cap.read()
        if aux % 2 != 0:
            if (success == True):
                contador+=1
                blob = cv2.dnn.blobFromImage(img, 1 / 255, (whT, whT), [0, 0, 0], 1, crop=False)
                net.setInput(blob)

                layerNames = net.getLayerNames()
                outputNames = [layerNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]
                outputs = net.forward(outputNames)

                tiempo, nombre = findObjects(outputs, img)
                if nombre != "":
                    if tiempo not in lista:
                        lista.append(tiempo)
                        cv2.imwrite(pathResult + str(nombre) + str(tiempo) + '.png', img)

                #cv2.imshow('Image', img)
                cv2.waitKey(1)
            else:
                print("Duration in seconds: %s" % (time.time() - start_time))
                break
            aux+=1
        else:
            aux+=1

#--------------------------------------------------!-------------------------------------------------#



#Method to difine and exec threads
def threadsLogic():

    thread_list = []

    #Create a threads
    for i in range(1, 3):
        # Instantiates the thread
        t = threading.Thread(target=resultsDetection, args=('TestCustomYOLO/temp/part%s.mp4' %i,'TestCustomYOLO/output/'))
        t2 = threading.Thread(target=showDetection, args=('TestCustomYOLO/temp/part%s.mp4' %i,))
        # Sticks the thread in a list so that it remains accessible
        thread_list.append(t)
        thread_list.append(t2)

    # Starts threads
    for thread in thread_list:
        thread.start()

    for thread in thread_list:
        thread.join()



def main():
    #Exec divideVideo
    print("----------------------------Start with divide video----------------------------")
    divideVideo('input/test1.mp4')
    print("--------------------------------End divide video--------------------------------")
    print("\n")
    print("----------------------------Start with threads of result----------------------------")
    #TODO:No está haciendo el detectar en tiempo real 2 veces con hilo, el result si
    threadsLogic()
    print("-----------------------------End with threads of result------------------------------")
    print("\n")

main()

