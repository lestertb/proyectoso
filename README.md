### Para qué es este repositorio? ###

Este repositorio es un ejemplo simple de la implementación de yolo con hilos y multiprocesamiento para el reconocimiento de armas y fuego en videos. Además, se implementó la posibilidad de generar solo las imágenes y guardar los resultados en una función, y otra para reproducir en varios videos a la vez el reconocimiento en tiempo real de yolo (este consume más recursos).

### Como puedo instalarlo? ###

Para empezar, es necesario tener una versión  `Python` entre `3.7` a `3.9`, también  es necesita clonar el presente directorio con el siguiente comando:

```console
    git clone https://lestertb@bitbucket.org/lestertb/proyectoso.git
```
Una vez descargado el proyecto es necesario descargar las dependencias, para hacerlo
es necesario estar posicionado sobre la carpeta del proyecto con una consola de línea
de comandos como `Git Bash`, `CMD` o `Powershell`, y ejecutar:

```console
    pip install -r requirements.txt
```

Tambien es necesario descargar el modelo que usará yolo para la deteccion de objetos, el cual lo puede descargar en el 
siguiente enlace [https://1drv.ms/u/s!Aj0l1DM1G5wOm0Vg2n7RI5Q4_ZOq?e=gGvvMy](https://1drv.ms/u/s!Aj0l1DM1G5wOm0Vg2n7RI5Q4_ZOq?e=gGvvMy). Una vez descargado el archivo coloquelo en la base del proyecto junto al archivo de `yolov3.cfg`

### Como puedo ejecutarlo? ###

Primero, en el archivo `YOLOMultiprocessing.py` existe variables globales que configuran el funcionamiento de la aplicación, a continuación, se detallan que hace cada variable:

1. VIDEO_EXTENSION: Especifica las extensiones que van a tomar en cuenta para ser analizadas
2. CPU_COUNT: Especifica la cantidad de instancias que va a crear python para analizar. Por defecto usa las de la CPU
3. MAX_VIDEO_DURATION: Indica en cuantos segundos va a separar el video que se desea analizar.
4. ANALYZE_VIDEO: Indica si va a hacer reconocimiento de video mostrando en ventana o por consola
5. LOTE_SIZE: Tamaño de en qué divide los lotes de imágenes para ser analizados.

Para empezar a usar el proyecto es necesario tener un video el cual analizar. Coloque cualquier video en la carpeta `input` y por medio de una consola de línea de comandos ejecute:

```console
    python YOLOMultiprocessing.py
```

este comando ejecutará el archivo python y empezará el proceso de división de trabajos e identificación de objetos en imágenes. Los resultados del análisis  se almacenarán en la carpeta `output` y tendrán un marco que identifica en que punto
de la imagen se encuentra el objeto.